corrector_names = ['k500.bpm.e2v4.4DT1', 'k500.bpm.e2v4.3DT1', 'k500.bpm.e2v4.5DT1','k500.bpm.e2v4.5DT2', 'k500.bpm.e2v4.5DT3', 'k500.bpm.e2v4.5DT4', 'k500.bpm.e2v4.5DT5', 'k500.bpm.e2v4.5DT6', 'k500.bpm.e2v4.5DT7', 'k500.bpm.e2v4.5DT8', 'k500.bpm.e2v4.5DT9', 'k500.bpm.e2v4.5DT10', 'k500.bpm.e2v4.5DT11', 'k500.bpm.e2v4.5DT12', 'k500.bpm.e2v4.5DT13','k500.bpm.p2v4.4DT1', 'k500.bpm.p2v4.3DT1', 'k500.bpm.p2v4.5DT1', 'k500.bpm.p2v4.5DT2', 'k500.bpm.p2v4.5DT3', 'k500.bpm.p2v4.5DT4', 'k500.bpm.p2v4.5DT5', 'k500.bpm.p2v4.5DT6', 'k500.bpm.p2v4.5DT7', 'k500.bpm.p2v4.5DT8', 'k500.bpm.p2v4.5DT9', 'k500.bpm.p2v4.5DT10', 'k500.bpm.p2v4.5DT11', 'k500.bpm.p2v4.5DT12', 'k500.bpm.p2v4.5DT13', 'k500.bpm.e2v2.4DT1', 'k500.bpm.e2v2.3DT1', 'k500.bpm.e2v2.5DT1', 'k500.bpm.e2v2.6PIC1', 'k500.bpm.e2v2.6PIC2', 'k500.bpm.e2v2.6PIC3','k500.bpm.e2v2.6PIC4', 'k500.bpm.e2v2.6PIC5', 'k500.bpm.e2v2.6PIC6', 'k500.bpm.e2v2.6PIC7', 'k500.bpm.e2v2.6PIC8', 'k500.bpm.e2v2.6PIC9', 'k500.bpm.e2v2.6PIC10', 'k500.bpm.e2v2.6PIC11', 'k500.bpm.e2v2.6PIC12', 'k500.bpm.e2v2.6PIC13', 'k500.bpm.e2v2.6PIC14', 'k500.bpm.e2v2.6PIC15', 'k500.bpm.e2v2.6PIC16', 'k500.bpm.e2v2.6PIC17', 'k500.bpm.e2v2.6PIC18', 'k500.bpm.e2v2.6PIC19', 'k500.bpm.e2v2.6PIC20','k500.bpm.e2v2.6PIC22', 'k500.bpm.e2v2.6PIC23', 'k500.bpm.e2v2.6PIC24', 'k500.bpm.p2v2.4DT1', 'k500.bpm.p2v2.3DT1', 'k500.bpm.p2v2.5DT1', 'k500.bpm.p2v2.6PIC1', 'k500.bpm.p2v2.6PIC2', 'k500.bpm.p2v2.6PIC3', 'k500.bpm.p2v2.6PIC4', 'k500.bpm.p2v2.6PIC5', 'k500.bpm.p2v2.6PIC6', 'k500.bpm.p2v2.6PIC7', 'k500.bpm.p2v2.6PIC8', 'k500.bpm.p2v2.6PIC9', 'k500.bpm.p2v2.6PIC10', 'k500.bpm.p2v2.6PIC11', 'k500.bpm.p2v2.6PIC12', 'k500.bpm.p2v2.6PIC13', 'k500.bpm.p2v2.6PIC14', 'k500.bpm.p2v2.6PIC15', 'k500.bpm.p2v2.6PIC16', 'k500.bpm.p2v2.6PIC17', 'k500.bpm.p2v2.6PIC18', 'k500.bpm.p2v2.6PIC19', 'k500.bpm.p2v2.6PIC20', 'k500.bpm.p2v2.6PIC22', 'k500.bpm.p2v2.6PIC23', 'k500.bpm.p2v2.6PIC24']
# corrector_variables = ['x', 'z', 'I', 'c', 'x0', 'z0', 'd', 'sz', 'sx']
corrector_variables = ['x', 'z', 'I', 'c']

invalid_channels = [
"k500.bpm.p2v2.6PIC3",
"k500.bpm.p2v2.6PIC6",
"k500.bpm.p2v2.6PIC8",
"k500.bpm.p2v2.6PIC14",
"k500.bpm.e2v2.6PIC3",
"k500.bpm.e2v2.6PIC6",
"k500.bpm.e2v2.6PIC8",
"k500.bpm.e2v2.6PIC14",

"k500.bpm.p2v4.5DT8",
"k500.bpm.e2v4.5DT8",
# "k500.bpm.e2v4.4DT1",
]
corrector_names = [x for x in corrector_names if x not in invalid_channels]

final_pickups = ["k500.bpm.e2v2.6PIC24", "k500.bpm.e2v4.5DT13", "k500.bpm.p2v2.6PIC24", "k500.bpm.p2v4.5DT13"]

vepp_3_common_names = ['drm', 'dsm', 'qd1', 'qf1n2', 'qf4', 'qd2', 'qd3', 'qf3', 'd3m4n5', 'd4m4n5', 'd3m1t3', 'd4m1t3', 'd5m1t4', 'd6m1t4', 'd6m5n6', 'd5m7n8']
vepp_2000_common_names = ['c1d2_z', 'c1f2_x', 'c1f1_x', 'c1d1_z', 'c2d2_z', 'c2f2_x', 'c2f1_x', 'c2d1_z', 'c3d2_z', 'c3f2_x', 'c3f1_x', 'c3d1_z', 'c4d2_z', 'c4f2_x', 'c4f1_x', 'c4d1_z', 'crm1', 'crm2', 'crm3', 'crm4', 'crm5','crm6', 'crm7', 'crm8']
vepp_3_names = ['c5kx3', 'c5kz4', 'c5kx5', 'c5kz6', 'c5kx7', 'c5kx8', 'c5kz9', 'c5kz11', 'c5kx12', 'c5m1', 'c5m2', 'c5m3', 'c5m4']
vepp_2000_names = ['c6kx4', 'c6kz5', 'c6kx6', 'c6kz7', 'c6kx8', 'c6kx9', 'c6kz10', 'c6kx11', 'c6kz12', 'c6kx13', 'c6kz14', 'c6kx15', 'c6kz16', 'c6kx17', 'c6kx18', 'c6kz19', 'c6kx20', 'c6kz21', 'c6kx22', 'c6kz23', 'c6kz24', 'c6m1', 'c6m2', 'c6m3', 'c6m4']


corrector_channels = []
for name in corrector_names:
	for variable in corrector_variables:
		corrector_channels.append(".".join([name, variable]))

e2v2_updated_channels = {}
e2v4_updated_channels = {}
p2v2_updated_channels = {}
p2v4_updated_channels = {}

vepp_3_common_channels = []
vepp_2000_common_channels = []
vepp_3_channels = []
vepp_2000_channels = []
channel_modes = ["Imes", "Iset"]
for channel_mode in channel_modes:
	vepp_3_common_channels += ["canhw:12.%s.%s" % (el, channel_mode) for el in vepp_3_common_names]
	vepp_2000_common_channels += ["canhw:12.rst2.%s.%s" % (el, channel_mode) for el in vepp_2000_common_names]
	vepp_3_channels += ["canhw:12.rst5.%s.%s" % (el, channel_mode) for el in vepp_3_names]
	vepp_2000_channels += ["canhw:12.rst5.%s.%s" % (el, channel_mode) for el in vepp_2000_names]

all_channels = []
# all_channels += ['cxhw:0.k500.modet']
all_channels += corrector_channels
all_channels += vepp_3_common_channels
all_channels += vepp_2000_common_channels
all_channels += vepp_3_channels
all_channels += vepp_2000_channels
print("%d channels total" % (len(all_channels)))


from collections import OrderedDict
channels_current_values = OrderedDict()
for channel in all_channels:
	channels_current_values[channel] = 0.0

import os
from os.path import isfile

data_folder = './data'
backup_folder = './data/backups'

log_file_name = 'k500_log.csv'
from time import localtime, strftime
time_str = strftime("%Y-%m-%d_%H:%M:%S", localtime())
log_file_name_backup = 'k500_log_%s.csv' % time_str

import numpy as np  
from shutil import copyfile
log_file_path = os.path.join(data_folder, log_file_name)
log_file_path_backup = os.path.join(backup_folder, log_file_name_backup)
if isfile(log_file_path):
	print("Backuped %s to %s" % (log_file_path, log_file_path_backup))
	copyfile(log_file_path, log_file_path_backup)
	log_file = open(log_file_path, 'a')
else:
	print("Created log file %s" % (log_file_path))
	log_file = open(log_file_path, 'w')
	header = ["epoch_time_mod_1e7"] + ["beam_regime"] + all_channels
	header_str = ",".join(header) + '\n'
	log_file.write(header_str)

regimes_count = np.zeros(4, dtype=np.int32)
def log_channel_values(channel, last_final_pickup_regime):
	pickup_regime_to_numeric = {
		"e2v2": 0,
		"e2v4": 1,
		"p2v2": 2,
		"p2v4": 3,
	}
	regimes_count[pickup_regime_to_numeric[last_final_pickup_regime]] += 1
	# print('Saved from regime', last_final_pickup_regime)
	print([(regime, regimes_count[pickup_regime_to_numeric[regime]]) for regime in pickup_regime_to_numeric], end='\r')

	values = np.array(list(channels_current_values.values()), dtype=np.float32).reshape([1, len(channels_current_values)])

	from time import localtime
	from calendar import timegm
	cur_time = timegm(localtime()) % 1e7

	log_array = np.insert(values, 0, pickup_regime_to_numeric[last_final_pickup_regime], axis = 1)
	log_array = np.insert(log_array, 0, cur_time, axis = 1)
	np.savetxt(log_file, log_array, delimiter = ',')


is_init = True
def set_channel_updated(channel):
	channel_group = None
	group_to_updated_channels = {
		"e2v2": e2v2_updated_channels,
		"e2v4": e2v4_updated_channels,
		"p2v2": p2v2_updated_channels,
		"p2v4": p2v4_updated_channels,
	}
	global is_init
	is_init = False
	for group in group_to_updated_channels:
		if group in channel.split('.'):
			channel_group = group_to_updated_channels[group]
			if not channel in channel_group:
				channel_group[channel] = 0
				is_init = True
			else:
				channel_group[channel] = 1

def all_channels_in_group_updated(channel):
	updated_status = []
	global e2v2_updated_channels, e2v4_updated_channels, p2v2_updated_channels, p2v4_updated_channels
	channel_groups = [e2v2_updated_channels, e2v4_updated_channels, p2v2_updated_channels, p2v4_updated_channels]
	for updated_channels_group in channel_groups:
		updated_status.append(all([updated for updated in updated_channels_group.values()]))
	if any(updated_status):
		e2v2_updated_channels = dict.fromkeys(e2v2_updated_channels, 0)
		e2v4_updated_channels = dict.fromkeys(e2v4_updated_channels, 0)
		p2v2_updated_channels = dict.fromkeys(p2v2_updated_channels, 0)
		p2v4_updated_channels = dict.fromkeys(p2v4_updated_channels, 0)
	channel_group = e2v4_updated_channels
	return any(updated_status)



import math
import time
now = time.time()
logged = True
last_final_pickup_regime = None
def update_callback(channel):
	global last_final_pickup_regime
	global now
	global logged
	global is_init
	
	channel_name = channel.name
	channels_current_values[channel_name] = channel.val
	if channel_name in corrector_channels:
		set_channel_updated(channel_name)
		if ".".join(channel_name.split('.')[:-1]) in final_pickups:
			now = time.time()
			logged = False
			last_final_pickup_regime = channel_name.split('.')[2]
	all_channels_in_group_updated(channel_name)
	if (not logged) and (not is_init):
		later = time.time()
		if later - now >= 1.5:
			log_channel_values(channel_name, last_final_pickup_regime)
			logged = True


import pycx4.pycda as cda
import sys
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

all_dchans = []
for channel in all_channels:
	dchan = cda.DChan(channel)
	dchan.valueMeasured.connect(update_callback)
	all_dchans.append(dchan)

cda.main_loop()